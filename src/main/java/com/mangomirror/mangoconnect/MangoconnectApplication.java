package com.mangomirror.mangoconnect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MangoconnectApplication {

	public static void main(String[] args) {
		SpringApplication.run(MangoconnectApplication.class, args);
	}

}
